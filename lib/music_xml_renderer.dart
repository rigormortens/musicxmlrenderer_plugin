library music_xml_renderer;

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';

import 'package:webview_flutter/webview_flutter.dart';

class MusicXMLRenderer {
  WebViewController? _controller;
  final StreamController<String> _musicXmlStream = StreamController<String>();

  final StreamController<bool> _stateStreamController = StreamController();
  late Stream<bool> stateStream;

  MusicXMLRenderer() {
    stateStream = _stateStreamController.stream;
  }

  Widget get view => MusicXMLRenderView(
        xmlStream: _musicXmlStream.stream,
        onControllerCreated: (ctrl) {
          _controller = ctrl;
        },
        onWebViewReady: () {
          _stateStreamController.add(true);
        },
      );

  _sendJSCommand(String js) {
    _controller?.runJavascript(js);
  }

  loadMusicXMLFile(String fileData) {
    _musicXmlStream.add(fileData);
  }

  loadMusicXML(String music) {
    _sendJSCommand("renderXmlString( \"${Uri.encodeFull(music)}\" );");
  }

  setOption(Map<dynamic, dynamic> opt) {
    print("set option $opt");
    _sendJSCommand("setOptions($opt);");
  }

  showCursor() {
    _sendJSCommand("showCursor();");
  }

  hideCursor() {
    _sendJSCommand("hideCursor();");
  }

  moveCursorToNext() {
    _sendJSCommand("moveCursorToNext();");
  }

  resetCursor() {
    _sendJSCommand("resetCursor();");
  }

  highlightNotesUnderCursor(List<int> notes) {
    _sendJSCommand("highlightNotesUnderCursor($notes)");
  }

  resetHighlights() {
    _sendJSCommand("resetHighlights();");
  }

  String _convertNSArrayToJsonArray(String nsArray) {
    nsArray = nsArray.replaceFirst("(", "[");
    nsArray = nsArray.replaceRange(nsArray.length - 1, null, "]");

    var allObjects = nsArray.split("\n");
    StringBuffer jsonBuffer = StringBuffer();
    for (var element in allObjects) {
      if (element.contains("=")) {
        var parts = element.split("=");
        jsonBuffer.write("\"${parts[0].trim()}\":${parts[1].split(";")[0].trim().replaceAll("\"", "")},");
      } else {
        jsonBuffer.write(element.trim());
      }
    }

    nsArray = jsonBuffer.toString();
    nsArray = nsArray.replaceAll(",}", "}");

    return nsArray;
  }

  Future<List<NoteEntry>> notes() async {
    var noteJson = await _controller?.runJavascriptReturningResult("iterate();");

    if (noteJson != null) {
      // iOS JS Array Hack
      if (noteJson.startsWith("(") && noteJson.endsWith(")")) {
        print("fix NS array");
        noteJson = _convertNSArrayToJsonArray(noteJson);
      }
      // debugPrint(noteJson);

      List<dynamic> notesData = const JsonDecoder().convert(noteJson);
      List<NoteEntry> notes = notesData.map((e) => NoteEntry.fromJson(e)).toList(growable: false);
      return notes;
    }
    return Future.error("Failed to get notes");
  }

  Future<int?> denominator() async {
    return _controller?.runJavascriptReturningResult("denominator();").then((value) => int.parse(value));
  }
}

class MusicXMLRenderView extends StatefulWidget {
  final Function(WebViewController?) onControllerCreated;
  final Function() onWebViewReady;
  final Stream<String> xmlStream;

  MusicXMLRenderView({
    Key? key,
    required this.onControllerCreated,
    required this.onWebViewReady,
    required this.xmlStream,
  });

  @override
  _MusicXMLRenderViewState createState() => _MusicXMLRenderViewState();
}

class _MusicXMLRenderViewState extends State<MusicXMLRenderView> {
  WebViewController? _webViewController;

  StreamSubscription? xmlSubscription;

  bool _didLoadPage = false;
  bool _pageWasLoaded = false;

  @override
  void initState() {
    xmlSubscription = widget.xmlStream.listen((xml) {
      _loadHtmlWithMusicXML(xml);
    });

    super.initState();
  }

  @override
  void dispose() {
    xmlSubscription?.cancel();
    super.dispose();
  }

  void _log(String text) {
    print(text);
    // ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(text)));
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onHorizontalDragUpdate: (_) {},
      onVerticalDragUpdate: (_) {},
      child: WebView(
        initialUrl: "",
        javascriptMode: JavascriptMode.unrestricted,
        gestureNavigationEnabled: false,
        debuggingEnabled: true,
        backgroundColor: Colors.transparent,
        zoomEnabled: false,
        onPageFinished: (value) {
          print("page loaded");
          _log("page loaded $value");

          if (!_didLoadPage) {
            _webViewController?.loadFlutterAsset("packages/music_xml_renderer/assets/index.html");
            _didLoadPage = true;
            _log("did load true");
          } else if (!_pageWasLoaded) {
            _pageWasLoaded = true;
            _log("load complete");
            widget.onWebViewReady();
          }
        },
        onWebResourceError: (err) {
          _log("webressource error $err");
        },
        onWebViewCreated: (webViewController) async {
          _log("complete $webViewController");
          widget.onControllerCreated(webViewController);
          _webViewController = webViewController;
        },
      ),
    );
  }

  _loadHtmlWithMusicXML(String musicXml) async {
    HttpServer server = await HttpServer.bind(InternetAddress.loopbackIPv4, 8080, shared: true);

    _log("start server $server @ ${InternetAddress.loopbackIPv4}");
    server.handleError((err) {
      _log("server error $err");
    });
    server.listen((HttpRequest request) async {
      request.response.statusCode = HttpStatus.ok;
      request.response.headers.add("Access-Control-Allow-Origin", "*");
      request.response.headers.add("Access-Control-Allow-Methods", "GET");
      request.response.headers.set("Content-Type", '${ContentType.binary.mimeType}; charset=utf-8');

      request.response.write(musicXml);
      await request.response.close();
      await server.close(force: true);
    });

    // _doFetchMusic = true;
    // _webViewController?.loadFlutterAsset("packages/music_xml_renderer/assets/index.html");
    _webViewController?.runJavascript("fetchMusic();");
  }
}

class NoteEntry {
  final int note;
  final double time;
  final double length;

  NoteEntry(this.note, this.time, this.length);

  NoteEntry.fromJson(Map<String, dynamic> json)
      : note = json['note'],
        time = json['time'].toDouble(),
        length = json['length'].toDouble();
}
