import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_midi_command/flutter_midi_command.dart';

class DeviceBrowser extends StatefulWidget {
  const DeviceBrowser({Key? key}) : super(key: key);

  @override
  DeviceBrowserState createState() => DeviceBrowserState();
}

class DeviceBrowserState extends State<DeviceBrowser> {
  StreamSubscription<String>? _setupSubscription;
  final MidiCommand _midiCommand = MidiCommand();

  @override
  void initState() {
    super.initState();

    _setup();
    _setupSubscription = _midiCommand.onMidiSetupChanged?.listen((data) {
      print("setup changed $data");
      setState(() {});
    });
  }

  _setup() async {
    await _midiCommand.startBluetoothCentral();

    await _midiCommand.waitUntilBluetoothIsInitialized();

    if (_midiCommand.bluetoothState == BluetoothState.poweredOn) {
      _midiCommand.startScanningForBluetoothDevices().catchError((err) {
        print("Error $err");
      });
    } else {
      print("BLE ERROR");
    }
  }

  @override
  void dispose() {
    _setupSubscription?.cancel();
    _midiCommand.stopScanningForBluetoothDevices();
    super.dispose();
  }

  IconData _deviceIconForType(String type) {
    switch (type) {
      case "native":
        return Icons.devices;
      case "network":
        return Icons.language;
      case "BLE":
        return Icons.bluetooth;
      default:
        return Icons.device_unknown;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(icon: const Icon(Icons.close), onPressed: () => Navigator.maybePop(context)),
        actions: <Widget>[
          IconButton(
              icon: const Icon(Icons.refresh),
              onPressed: () {
                _midiCommand.startScanningForBluetoothDevices();
                setState(() {});
              }),
        ],
        title: const Text('MIDI Devices'),
      ),
      body: FutureBuilder(
        future: _midiCommand.devices,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData && snapshot.data != null) {
            var devices = snapshot.data as List<MidiDevice>;
            return ListView.builder(
              itemCount: devices.length,
              itemBuilder: (context, index) {
                MidiDevice device = devices[index];

                return ListTile(
                  title: Text(
                    device.name,
                    style: Theme.of(context).textTheme.headline5,
                  ),
                  subtitle: Text("ins:${device.inputPorts.length} outs:${device.outputPorts.length}"),
                  leading: Icon(device.connected ? Icons.radio_button_on : Icons.radio_button_off),
                  trailing: Icon(_deviceIconForType(device.type)),
                  onTap: () {
                    if (device.connected) {
                      print("disconnect");
                      _midiCommand.disconnectDevice(device);
                    } else {
                      print("connect");
                      _midiCommand.connectToDevice(device).then((_) => print("device connected async ${device.name} : ${device.connected}"));
                    }
                  },
                );
              },
            );
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}
