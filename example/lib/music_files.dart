import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';

class MusicFiles {
  factory MusicFiles() {
    _instance ??= MusicFiles._();
    return _instance!;
  }
  static MusicFiles? _instance;

  List<MusicFile>? _files;
  late Future<List<MusicFile>> _filesLoading;

  MusicFiles._() {
    _filesLoading = _loadFiles();
    _filesLoading.then((books) {
      _files = books;
    });
  }

  Future<List<MusicFile>> get books {
    if (_files != null) {
      return Future.value(_files);
    } else {
      return _filesLoading;
    }
  }

  Future<List<MusicFile>> _loadFiles() async {
    final manifestContent = await rootBundle.loadString('AssetManifest.json');
    final Map<String, dynamic> manifestMap = json.decode(manifestContent);
    List<String> filePaths = manifestMap.keys.where((String key) => key.contains('music_files/')).where((String key) => key.contains('.musicxml')).toList();

    List<MusicFile> files = [];
    await Future.forEach<String>(filePaths, (path) async {
      var fp = Uri.decodeFull(path);
      var bytes = await rootBundle.loadString(fp);
      files.add(MusicFile(name: fp.split("/").last, fileData: bytes));
    });

    return files;
  }
}

class MusicFile {
  final String name;
  final String fileData;

  MusicFile({required this.name, required this.fileData});
}
