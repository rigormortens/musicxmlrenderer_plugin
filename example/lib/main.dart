import 'package:example/device_browser.dart';
import 'package:example/plugin.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MusicXMLRenderer Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'MusicXMLRenderer Demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          IconButton(onPressed: _showDeviceBrowser, icon: const Icon(Icons.devices)),
        ],
      ),
      body: const Plugin(),
    );
  }

  _showDeviceBrowser() {
    showModalBottomSheet(context: context, builder: (_) => const DeviceBrowser()).whenComplete(
      () {
        print('device browser complete');
      },
    );
  }
}
