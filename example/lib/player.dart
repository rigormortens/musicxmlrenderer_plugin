import 'dart:async';
import 'dart:developer';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_midi_command/flutter_midi_command.dart';
import 'package:flutter_midi_command/flutter_midi_command_messages.dart';
import 'package:music_xml_renderer/music_xml_renderer.dart';
import 'package:collection/collection.dart';

class Player extends StatefulWidget {
  final MusicXMLRenderer renderer;

  const Player({
    Key? key,
    required this.renderer,
  }) : super(key: key);

  @override
  _PlayerState createState() => _PlayerState();
}

class _PlayerState extends State<Player> {
  int _bpm = 100;
  int _denominator = 4;
  bool _isPlaying = false;
  bool _sendNotesOnMIDI = false;

  final MidiCommand _midiCommand = MidiCommand();
  StreamSubscription? _midiSub;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _midiSub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 210,
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                flex: 4,
                child: Slider(
                  value: _bpm.toDouble(),
                  max: 300,
                  min: 30,
                  onChanged: (newValue) {
                    setState(() {
                      _bpm = newValue.round();
                    });
                    // _setMetroTempo(newValue.round());
                  },
                ),
              ),
              Flexible(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Text("BPM $_bpm"),
                ),
              ),
            ],
          ),
          Row(
            children: [
              const Padding(
                padding: EdgeInsets.all(12.0),
                child: Text("Send notes on MIDI"),
              ),
              Switch(
                  value: _sendNotesOnMIDI,
                  onChanged: (newValue) {
                    setState(() {
                      _sendNotesOnMIDI = newValue;
                    });
                  }),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Center(
            child: ElevatedButton(
              onPressed: () {
                if (_isPlaying) {
                  _stopPlaying();
                } else {
                  _startPlaying();
                }
              },
              style: ButtonStyle(
                elevation: MaterialStateProperty.all(1),
                shape: MaterialStateProperty.all(const CircleBorder()),
              ),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Icon(_isPlaying ? Icons.stop_rounded : Icons.play_arrow_rounded, size: 40),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _startPlaying() async {
    var denom = await widget.renderer.denominator();

    setState(() {
      _denominator = denom ?? 0;
      _isPlaying = true;
    });

    widget.renderer.resetCursor();
    widget.renderer.showCursor();

    var notes = List<NoteEntry>.from(await widget.renderer.notes());

    // Add metro beat to notegroups

    var lastTime = notes.last.time;
    // print("lastTime $lastTime $_denominator");

    for (int i = 0; i <= _denominator; i++) {
      notes.add(NoteEntry(-1, -i / _denominator, 0));
    }

    for (int i = 0; i <= lastTime * _denominator; i++) {
      notes.add(NoteEntry(-1, i / _denominator, 0));
    }

    var noteGroups = groupBy(notes, (NoteEntry e) => e.time);
    _playNotes(noteGroups);
    // _setMetroTempo(_bpm);
    // _setMetroActive(true);
  }

  _stopPlaying() {
    setState(() {
      _isPlaying = false;
    });
    // _setMetroActive(false);
  }

  List<NoteEntry>? notesToPlay;

  Map<int, List<NoteEntry>>? _noteTickGroups;

  int countdown = 0;

  _playNotes(Map<double, List<NoteEntry>> noteGroups) async {
    widget.renderer.resetCursor();
    // widget.renderer.showCursor();
    widget.renderer.resetHighlights();

    double prevTime = -1; // 1 Bar count in
    List<NoteEntry>? notesToPlay;

    _midiSub = _midiCommand.onMidiDataReceived?.listen((packet) {
      print("received midi ${packet.data}");

      var data = packet.data;
      if (data.length == 3) {
        var status = data[0];
        var note = data[1];
        var rawStatus = status & 0xF0; // without channel

        if (rawStatus == 0x90) {
          // print("$note on notesToPlay ${notesToPlay?.map((e) => e.note).join(",")}");

          if (notesToPlay != null) {
            var playedNotes = [];
            for (var element in notesToPlay!) {
              if (element.note == note) {
                playedNotes.add(element);
              }
            }
            notesToPlay?.removeWhere((element) => playedNotes.contains(element));
          }
        }
      }
    });

    var entries = noteGroups.entries.sorted((a, b) => a.key.compareTo(b.key));

    await Future.forEach(entries, (MapEntry<double, List<NoteEntry>> entry) async {
      if (!_isPlaying) return;

      var deltaTime = (entry.key - prevTime).abs();
      await Future.delayed(Duration(milliseconds: (deltaTime * wholeNoteLength).round()));

      if (!_isPlaying) return;

      if (notesToPlay != null) {
        _highlightMissingNotes(notesToPlay);
      }

      if (entry.value.any((element) => element.note == -1)) {
        _playMetroBeat();
      }

      var notes = entry.value.where((entry) => entry.note >= 0).toList();
      if (notes.isNotEmpty) {
        if (notesToPlay != null) {
          widget.renderer.moveCursorToNext();
        }
        notesToPlay = notes;
        if (_sendNotesOnMIDI) {
          _playNotesOnPiano(notesToPlay);
        }
      }

      prevTime = entry.key;
    });
  }

  int get wholeNoteLength => ((60 * _denominator) / _bpm).round() * 1000;

  _playNotesOnPiano(List<NoteEntry>? notes) {
    notes?.forEach((element) async {
      var on = NoteOnMessage(channel: 0, note: element.note, velocity: 100);
      on.send();

      await Future.delayed(Duration(milliseconds: (element.length * wholeNoteLength).round() - 50));

      var off = NoteOffMessage(channel: 0, note: element.note);
      off.send();
    });
  }

  _highlightMissingNotes(List<NoteEntry>? notes) {
    if (notes != null) {
      widget.renderer.highlightNotesUnderCursor(notes.map((e) => e.note).toList(growable: false));
    }
  }

  //#region GEWA Piano API
  // _setMetroActive(bool value) {
  //   _midiCommand.sendData(_bytesForCommand(0x36, 0x1E, value ? 1 : 0));
  // }

  // _setMetroTempo(int value) {
  //   var high = (value & 0x3F80) >> 7;
  //   var low = value & 0x7F;
  //   _midiCommand.sendData(_bytesForCommand(0x36, 0x11, high));
  //   _midiCommand.sendData(_bytesForCommand(0x36, 0x12, low));
  //   _midiCommand.sendData(_bytesForCommand(0x36, 0x1F, 0)); //Pattern
  // }

  _playMetroBeat() {
    _midiCommand.sendData(_bytesForCommand(0x36, 0x1E, 0x11));
  }

  Uint8List _bytesForCommand(int msb, int lsb, int value, {int channel = 0}) {
    var bytes = Uint8List(7);
    bytes[0] = 0xB0 + channel;
    bytes[1] = 0x63;
    bytes[2] = msb;
    bytes[3] = 0x62;
    bytes[4] = lsb;
    bytes[5] = 0x06;
    bytes[6] = value & 0x7F;
    return bytes;
  }
  //#endregion
}
