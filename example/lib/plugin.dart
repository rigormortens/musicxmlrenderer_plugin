import 'dart:async';

import 'package:example/music_files.dart';
import 'package:example/player.dart';
import 'package:flutter/material.dart';
import 'package:music_xml_renderer/music_xml_renderer.dart';

enum Clef {
  treble,
  bass,
}

enum Direction {
  up,
  down,
  both,
}

enum BassClefDirection {
  parallel,
  counter,
}

class Plugin extends StatefulWidget {
  const Plugin({Key? key}) : super(key: key);

  @override
  _PluginState createState() => _PluginState();
}

class _PluginState extends State<Plugin> {
  final MusicXMLRenderer _rend = MusicXMLRenderer();

  MusicFile? _selectedFile;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text("MusicXML File"),
              Card(
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: FutureBuilder<List<MusicFile>>(
                    future: MusicFiles().books,
                    builder: (context, snapshot) {
                      if (!snapshot.hasData) {
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      }

                      return PopupMenuButton<MusicFile>(
                        onSelected: (value) {
                          setState(() {
                            _selectedFile = value;
                          });
                          _rend.loadMusicXMLFile(_selectedFile!.fileData);
                        },
                        child: Text(_selectedFile?.name.split("/").last ?? "Select a file"),
                        itemBuilder: (context) {
                          return snapshot.data!
                              .map(
                                (e) => PopupMenuItem(
                                  value: e,
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(e.name),
                                  ),
                                ),
                              )
                              .toList();
                        },
                      );
                    },
                  ),
                ),
              )
            ],
          ),
        ),
        Row(
          children: [
            MaterialButton(
              onPressed: () {
                _rend.loadMusicXML(_generateMusicXML());
//                     '''
// <?xml version="1.0" encoding="UTF-8" standalone="no"?>
// <score-partwise version="4.0">
// <part-list>
// <score-part id="P1">
// </score-part>
// </part-list>
// <part id="P1">
// <measure number="1">
// <attributes>
// <divisions>1</divisions>
// <staves>2</staves>
// <key>
// <fifths>0</fifths>
// </key>
// <clef number="1">
// <sign>G</sign>
// <line>2</line>
// </clef>
// <clef number="2">
// <sign>F</sign>
// <line>4</line>
// </clef>
// </attributes>
// <note>
//   <pitch>
//     <step>C</step>
//     <octave>4</octave>
//     <alter>-1</alter>
//   </pitch>
//   <duration>4</duration>
//   <type>whole</type>
//   <staff>1</staff>
//   <voice>1</voice>
//   <chord/>
//   <notations>
//    <technical>
//       <fingering placement="above">1</fingering>
//    </technical>
//   </notations>
// </note>
// <note>
//   <pitch>
//     <step>C</step>
//     <octave>3</octave>
//     <alter>0</alter>
//   </pitch>
//   <duration>4</duration>
//   <type>whole</type>
//   <staff>2</staff>
//   <voice>2</voice>
//   <chord/>
//   <notations>
//    <technical>
//       <fingering placement="below">3</fingering>
//    </technical>
//   </notations>
// </note>
// </measure>
// </part>
// </score-partwise>''');
              },
              child: Text("String"),
            ),
            MaterialButton(
                child: Text("dark"),
                onPressed: () {
                  _rend.setOption({"darkMode": true});
                }),
            MaterialButton(
                child: Text("light"),
                onPressed: () {
                  _rend.setOption({"darkMode": false});
                }),
          ],
        ),
        Expanded(child: _rend.view),
        Player(renderer: _rend),
      ],
    );
  }

  String _generateMusicXML() {
    var majorSteps = [2, 2, 1, 2, 2, 2, 1];

    var cDurStartNote = 48;
    var dDurStartNote = 50;
    var eDurStartNote = 52;

    var octaves = 3;

// var durMidi = [48, 50, 52, 53, 55, 57, 59, 60, 62, 64, 65, 67, 69, 71, 72 , 74, 76, 77, 79, 81, 83, 84];
    var cDurFingerings = [1, 2, 3, 1, 2, 3, 4]; // 5 (+1)

    StringBuffer buf = StringBuffer();

    // Header
    buf.write('''
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<score-partwise version="4.0">
  <part-list>
    <score-part id="P1">
    </score-part>
  </part-list>
  <part id="P1">
    <measure>
''');

    // Clef / stave

    buf.write('''
      <attributes>
        <divisions>1</divisions>
        <staves>2</staves>
        <key>
          <fifths>0</fifths>
        </key>
        <clef number="1">
          <sign>G</sign>
          <line>2</line>
        </clef>
        <clef number="2">
          <sign>F</sign>
          <line>4</line>
        </clef>
      </attributes>
''');

    // Notes in scale
    buf.write(_generateClefNotes(cDurStartNote, octaves, majorSteps, cDurFingerings, Clef.treble, Direction.both));
    buf.write(_generateClefNotes(cDurStartNote - 24, octaves, majorSteps, cDurFingerings, Clef.bass, Direction.both));

/*
    // Upwards clef 1
    var noteValue = cDurStartNote;
    var noteCount = (8 * octaves) - (octaves - 1);
    for (int i = 0; i < noteCount; i++) {
      var label = midiToNote(noteValue);
      String step = label.substring(0, 1);

      var beam;
      switch (i % 7) {
        case 1:
        case 3:
          beam = "begin";
          break;
        case 2:
        case 6:
          beam = "end";
          break;
        case 4:
        case 5:
          beam = "continue";
          break;
      }

      buf.write('''
<note>
        <pitch>
          <step>$step</step>
          <octave>${midiOctave(noteValue) + 1}</octave>
          <alter>0</alter>
        </pitch>
          <duration>${i % 7 == 0 ? "4" : "2"}</duration>
        <type>${i % 7 == 0 ? "quarter" : "eighth"}</type>
        <staff>1</staff>
<beam number="1">$beam</beam>

  <notations>
   <technical>
      <fingering placement="above">${i == (noteCount - 1) ? cDurFingerings[6] + 1 : cDurFingerings[i % 7]}</fingering>
   </technical>
  </notations>
      </note>
''');

      // Increase note value by step size
      if (i < (noteCount - 1)) {
        noteValue += majorSteps[i % 7];
      }

      // New measure after 7 notes
      if (i % 7 == 6) {
        buf.write('''</measure>
        <measure>''');
      }
    }

    //downwards clef 1
    noteCount = (8 * octaves) - (octaves);
    for (int i = noteCount - 1; i >= 0; i--) {
      noteValue -= majorSteps[i % 7];

      var label = midiToNote(noteValue);

      String step = label.substring(0, 1);

      var beam;
      switch (i % 7) {
        case 6:
        case 4:
          beam = "begin";
          break;
        case 5:
        case 1:
          beam = "end";
          break;
        case 2:
        case 3:
          beam = "continue";
          break;
      }

      buf.write('''
<note>
        <pitch>
          <step>$step</step>
          <octave>${midiOctave(noteValue) + 1}</octave>
          <alter>0</alter>
        </pitch>
          <duration>${i % 7 == 0 ? "4" : "2"}</duration>
        <type>${i % 7 == 0 ? "quarter" : "eighth"}</type>
        <staff>1</staff>
<beam number="1">$beam</beam>
  <notations>
   <technical>
      <fingering placement="above">${cDurFingerings[i % 7]}</fingering>
   </technical>
  </notations>
      </note>
''');

      // New measure after 7 notes
      if (i % 7 == 1) {
        buf.write('''</measure><measure>''');
      }
    }
*/
    // Footer
    buf.write('''
  </part>
</score-partwise>
''');

    return buf.toString();
  }

  String _generateClefNotes(int startNote, int octaves, List<int> steps, List<int> fingerings, Clef clef, Direction direction) {
    StringBuffer buf = StringBuffer();

    // Upwards clef 1
    var noteValue = startNote;

    var noteCount = (8 * octaves) - (octaves - 1);

    if (direction == Direction.up || direction == Direction.both) {
      for (int i = 0; i < noteCount; i++) {
        var label = midiToNote(noteValue);
        String step = label.substring(0, 1);

        var beam;
        switch (i % 7) {
          case 1:
          case 3:
            beam = "begin";
            break;
          case 2:
          case 6:
            beam = "end";
            break;
          case 4:
          case 5:
            beam = "continue";
            break;
        }

        buf.write('''
<note>
        <pitch>
          <step>$step</step>
          <octave>${midiOctave(noteValue) + 1}</octave>
          <alter>0</alter>
        </pitch>
        
          <duration>${i % 7 == 0 ? "4" : "2"}</duration>
        <type>${i % 7 == 0 ? "quarter" : "eighth"}</type>
        <staff>${clef == Clef.treble ? 1 : 2}</staff>     
  <voice>${clef == Clef.treble ? 1 : 2}</voice>
  
<beam number="1">$beam</beam>
  <notations>
   <technical>  
      <fingering placement="${clef == Clef.treble ? "above" : "below"}">${i == (noteCount - 1) ? fingerings[6] + 1 : fingerings[i % 7]}</fingering>
   </technical>
  </notations>
      </note>
''');

        // Increase note value by step size
        if (i < (noteCount - 1)) {
          noteValue += steps[i % 7];
        }

        // New measure after 7 notes
        if (i % 7 == 6) {
          buf.write('''</measure><measure>''');
        }
      }
    }

    if (direction == Direction.down || direction == Direction.both) {
      noteValue = startNote + (octaves * 12);
      noteCount = (8 * octaves) - (octaves);

      if (direction == Direction.down) {
        noteValue -= 1;
        noteCount += 1;
      }

      for (int i = noteCount - 1; i >= 0; i--) {
        noteValue -= steps[i % 7];

        var label = midiToNote(noteValue);

        String step = label.substring(0, 1);

        var beam;
        switch (i % 7) {
          case 6:
          case 4:
            beam = "begin";
            break;
          case 5:
          case 1:
            beam = "end";
            break;
          case 2:
          case 3:
            beam = "continue";
            break;
        }

        buf.write('''<note>
        <pitch>
          <step>$step</step>
          <octave>${midiOctave(noteValue) + 1}</octave>
          <alter>0</alter>
        </pitch>
          <duration>${i % 7 == 0 ? "4" : "2"}</duration>
        <type>${i % 7 == 0 ? "quarter" : "eighth"}</type>
        <staff>${clef == Clef.treble ? 1 : 2}</staff>     
  <voice>${clef == Clef.treble ? 1 : 2}</voice>
  
<beam number="1">$beam</beam>
  <notations>
   <technical>  
      <fingering placement="${clef == Clef.treble ? "above" : "below"}">${(direction == Direction.down && i == (noteCount - 1)) ? fingerings[6] + 1 : fingerings[i % 7]}</fingering>
   </technical>
  </notations>
      </note>''');

        // New measure after 7 notes
        if (i % 7 == 1) {
          buf.write('''</measure><measure>''');
        }
      }
    }
    // Footer
    buf.write('</measure>');

    return buf.toString();
  }

  static const _noteNames = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"];

  static String midiToNote(int midiValue) {
    var noteIndex = midiValue % 12;
    String name = _noteNames[noteIndex];
    return name;
  }

  static int midiOctave(int midiValue) {
    return (midiValue / 12).floor() - 1;
  }
}
